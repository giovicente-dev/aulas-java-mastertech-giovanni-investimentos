package br.com.itau;

public class Calculador {
    private double valorCalculado;

    public double calcularValor(Investimento investimento){
        return valorCalculado = investimento.getValor() * investimento.getTAXA();
    }
}
