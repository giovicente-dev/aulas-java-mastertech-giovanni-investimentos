package br.com.itau;

import java.util.Scanner;

public class Leitor {
    private static Scanner scanValor, scanMeses;

    public static double lerValor(){
        scanValor = new Scanner(System.in);
        System.out.print("Digite o valor a ser investido por mês: ");
        double valor = scanValor.nextDouble();
        return valor;
    }

    public static int lerMeses(){
        scanMeses = new Scanner(System.in);
        System.out.print("Digite o número de meses de investimento: ");
        int meses = scanMeses.nextInt();
        return meses;
    }
}
