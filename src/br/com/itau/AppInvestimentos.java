package br.com.itau;

public class AppInvestimentos {
    public static void main(String[] args) {
        double valor = Leitor.lerValor();
        int meses = Leitor.lerMeses();

        Investimento investimento = new Investimento(valor, meses);
        Impressora.imprimir(Simulador.simularInvestimento(investimento));
    }
}
