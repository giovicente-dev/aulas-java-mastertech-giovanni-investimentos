package br.com.itau;

public class Simulador {
    public static double simularInvestimento(Investimento investimento){
        double valorSimulado = 0;
        Calculador calculador = new Calculador();

        for(int i = 0; i < investimento.getMeses(); i++){
            valorSimulado = (investimento.getValor()) + (valorSimulado + calculador.calcularValor(investimento));
        }

        return valorSimulado;
    }
}
