package br.com.itau;

public class Investimento {
    private double valor;
    private int meses;
    private final double TAXA = 0.007;

    public Investimento(double valor, int meses) {
        this.valor = valor;
        this.meses = meses;
    }

    public double getValor() {
        return valor;
    }

    public int getMeses() {
        return meses;
    }

    public double getTAXA() {
        return TAXA;
    }
}
